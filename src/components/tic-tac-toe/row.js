import React from "react";
import Cell from "./cell";

export default class Row extends React.Component {
  render() {
    const { row, rowIndex, play } = this.props;
    return (
      <li className="clearFloat">
        {row.map((cell, index) => {
          return (
            <Cell
              cell={cell}
              colIndex={index}
              rowIndex={rowIndex}
              key={index}
              play={play}
            />
          );
        })}
      </li>
    );
  }
}
