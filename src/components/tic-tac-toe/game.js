import React from "react";
import Row from "./row";
const initState = {
  game: [[null, null, null], [null, null, null], [null, null, null]],
  end_game: false,
  winner: null,
  current_player: "O"
};
export default class Game extends React.Component {
  state = { ...initState };
  // end game condition
  // horizontal - each of the 3 rows will have same character - O/X - can be any row
  // vertical - each of the column (row[ith]) have the same character - 0/X - can be any column
  // col - row[0][0], row[1][0], row[2][0]
  // diagonal - row[0][0] = row[1][1] = row[2][2] ||
  // diagonal - row[0][2] = row[1][1] = row[2][0]
  // return the player
  checkHorizontal = rowList => {
    for (let i = 0; i < rowList.length; i++) {
      const rowIsFull = rowList[i].reduce((acc, val) => {
        return (acc = !!val && acc);
      }, true);
      if (
        rowIsFull &&
        rowList[i][0] === rowList[i][1] &&
        rowList[i][1] === rowList[i][2]
      ) {
        return true;
      }
    }
    return false;
  };
  checkVertical = rowList => {
    const colLength = rowList[0].length;
    for (let i = 0; i < colLength; i++) {
      const colIsFull = !!rowList[0][i] && !!rowList[1][i] && !!rowList[2][i];
      if (
        colIsFull &&
        rowList[0][i] === rowList[1][i] &&
        rowList[1][i] === rowList[2][i]
      ) {
        return true;
      }
    }
    return false;
  };

  checkDiagonal = rowList => {
    const diagonalIsFull =
      (!!rowList[0][0] && !!rowList[1][1] && !!rowList[2][2]) ||
      (!!rowList[0][2] && !!rowList[1][1] && !!rowList[2][0]);
    const diagnonalCheck =
      (rowList[0][0] === rowList[1][1] && rowList[1][1] === rowList[2][2]) ||
      (rowList[0][2] === rowList[1][1] && rowList[1][1] === rowList[2][0]);
    if (diagonalIsFull && diagnonalCheck) {
      return true;
    }
    return false;
  };

  checkEndGame = game => {
    if (
      this.checkHorizontal(game) ||
      this.checkVertical(game) ||
      this.checkDiagonal(game)
    ) {
      return this.state.current_player;
    }
    return false;
  };
  getNextPlayer(currentPlayer) {
    if (!currentPlayer) return "O";
    if (currentPlayer === "O") return "X";
    if (currentPlayer === "X") return "O";
  }
  play = (rowIndex, colIndex) => {
    const { game, current_player, winner } = this.state;
    if (winner) return;
    const oldRow = game[rowIndex];
    const newRow = [
      ...oldRow.slice(0, colIndex),
      current_player,
      ...oldRow.slice(colIndex + 1, oldRow.length)
    ];
    const newGame = [
      ...game.slice(0, rowIndex),
      newRow,
      ...game.slice(rowIndex + 1, game.length)
    ];
    const haveWinner = this.checkEndGame(newGame);
    if (haveWinner) {
      this.setState({
        game: newGame,
        current_player: this.getNextPlayer(null),
        winner: haveWinner,
        end_game: true
      });
    } else {
      this.setState({
        game: newGame,
        current_player: this.getNextPlayer(current_player)
      });
    }
  };
  reset = () => {
    this.setState({ ...initState });
  };
  render() {
    const { game, winner, current_player } = this.state;
    return (
      <React.Fragment>
        {current_player}'s turn
        <div>
          {game.map((row, index) => {
            return (
              <Row row={row} rowIndex={index} key={index} play={this.play} />
            );
          })}
        </div>
        {winner && `Winner is ${winner}`}
        <br />
        <button onClick={this.reset}>restart</button>
      </React.Fragment>
    );
  }
}
