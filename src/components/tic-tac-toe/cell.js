import React from "react";

export default class Cell extends React.Component {
  render() {
    const { cell, rowIndex, colIndex, play } = this.props;
    // cell data - 0, 1, null
    if (cell === null)
      return (
        <div
          className="cell"
          onClick={() => {
            play(rowIndex, colIndex);
          }}
        />
      );
    return <div className="cell">{cell}</div>;
  }
}
