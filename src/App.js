import React, { Component } from "react";
import "./App.css";
import Game from "./components/tic-tac-toe/game";

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <h3>TIC TAC TOE</h3>
        <Game />
      </React.Fragment>
    );
  }
}

export default App;
